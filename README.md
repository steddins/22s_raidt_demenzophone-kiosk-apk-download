# Apks für Demenzophone & Konfigurationsapp


## Projektbeschreibung

Dieses Repository enthält die Apks für die beiden Applikationen "Demenzophone" und "Konfigurationsapp". 
Die beiden Apks sind zurückzuführen auf den Tag "v1.0" in den jeweiligen Repositories.

Demenzophone:
Das Demenzophone ist eine seniorentaugliche App für demenzbetroffene Personen, welche die Benutzeroberfläche eines herkömmlichen Smartphones reduziert. Hauptsächlich kann der Senior nur noch Anrufe führen und entgegennehmen, um komplexe Benutzeroberflächen und Navigationsführungen zu vermeiden. Das Demenzophone läuft in einem sogenannten Kiosk-Mode, welcher die Applikation auf den Bildschirm des Smartphones anpinnt. Für die Angehörigen des Seniors oder demenzbetroffenen Person gibt es die Möglichkeit, Konfigurationseinstellungen an der App vorzunehmen, um eine einfache Interaktion mit dem Gerät zu ermöglichen.

Konfigurationsapp:
Die Konfigurationsapp ist eine App für Angehörige einer demenzbetroffenen Person. Hauptsächlich kann der Angehörige bestimmte Konfigurationseinstellungen auswählen und über einen Button automatisch an das Demenzophone senden. Die Gestaltung der Konfigurationsapp gleicht dem Konfigurationsmodus innerhalb des Demenzophone und bietet dadurch die identischen Einstellungsmöglichkeiten über eine Benutzeroberfläche. Nach der Auswahl einer Konfigurationseinstellung werden die Änderungen über eine SMS an das Demenzophone gesendet, um die Interaktion der demenzbetroffenen Person anzupassen und zu verbessern.

Hinweis:
Die Demenzophone-Apk ist hier abgelegt, damit eine Erstinstallierung über einen QR-Code erfolgen kann. 
Für den Code sowie Anleitung zur Installation und Nutzung wird auf die beiden nachfolgenden Repositories verwiesen. Hierfür müssen Sie den Zugriff anfragen.

• Demenzophone: https://gitlab.reutlingen-university.de/metib_steds_bt/22s_raidt_demenzophone-kiosk

• Konfigurationsapp: https://gitlab.reutlingen-university.de/metib_steds_bt/22s_raidt_demenzophone-remote-control



### Grundvoraussetzungen

Für den Nutzen und die Installation gibt es bestimmte Voraussetzung die eingehaltene werden müssen.

• Für das Demenzophone muss das zu verwendete Smartphone eine Android-Betriebssystem von mindestens Version 6 besitzen.

• Für das Demenzophone benötigen Sie eine aktive SIM-Karte, um das Demenzophone im vollen Umfang nutzen zu können. 

• Für das Demenzophone muss das zu verwendete Smartphone auf die Werkseinstellungen zurückgesetzt werden.

• Für das Demenzophone dürfen bei der ersten Nutzung nach einer Zurücksetzung keine Konten hinzugefügt werden, somit müssen alle Schritte übersprungen werden (Installation über GitLab) 

• Für die Installation über GitLab benötigen Sie einen Computer und eine Internetverbindung. 

• Für die Installation über den QR-Code benötigen Sie eine Internetverbindung.

• Für die Installation über den QR-Code benötigen Sie ein Smartphone mit mindestens Android Version 7.



## Kontakt

Name: Mick Raidt - Email: Mick.Raidt@Student.Reutlingen-University.DE

Projekt Link: https://gitlab.reutlingen-university.de/steddins/22s_raidt_demenzophone-kiosk-apk-download



